import os
import logging

import psycopg2
import psycopg2.extensions
from pymongo import MongoClient
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, Float, MetaData, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# ������� �� Psycopg2
# --------------------------------------------------------------

logger.info("������ ����������� � Postgres")
params = {
    "host": os.environ['APP_POSTGRES_HOST'],
    "port": os.environ['APP_POSTGRES_PORT'],
    "user": 'postgres'
}
conn = psycopg2.connect(**params)

# �������������� ���������
psycopg2.extensions.register_type(
    psycopg2.extensions.UNICODE,
    conn
)
conn.set_isolation_level(
    psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
)
cursor = conn.cursor()

# ��� ��� �����
# -------------
# ������� movies_top
# movieId (id ������), ratings_num(����� ���������), ratings_avg (������� ������� ������)

sql_str = "CREATE TABLE movies_top AS (SELECT movieId, count(1) as ratings_num, avg(rating) as ratings_avg FROM ratings GROUP BY movieId)"

# -------------

cursor.execute(sql_str)
conn.commit()

# �������� - ��������� ������
cursor.execute("SELECT * FROM movies_top LIMIT 10")
logger.info(
    "��������� ������ �� ������� movies_top: (movieId, ratings_num, ratings_avg)\n{}".format(
        [i for i in cursor.fetchall()])
)


# ������� �� SQLAlchemy
# --------------------------------------------------------------
Base = declarative_base()


class MoviesTop(Base):
    __tablename__ = 'movies_top'

    movieid = Column(Integer, primary_key=True)
    ratings_num = Column(Float)
    ratings_avg = Column(Float)

    def __repr__(self):
        return "<User(movieid='%s', ratings_num='%s', ratings_avg='%s')>" % (self.movieid, self.ratings_num, self.ratings_avg)


# ������ ������

engine = create_engine('postgresql://postgres:@{}:{}'.format(os.environ['APP_POSTGRES_HOST'], os.environ['APP_POSTGRES_PORT']))
Session = sessionmaker(bind=engine)
session = Session()


# --------------------------------------------------------------
# ��� ��� �����
# �������� ������� � �������� ������ 15 ������ (����������� filter)
# � ������� ������� ������ 3.5 (filter ��� ���)
# ��������������� �� �������� �������� (����������� order_by())
# id ������ �������� ����� ��������� � ������ top_rated_content_ids

top_rated_query = session.query(MoviesTop)

logger.info("������� �� top_rated_query\n{}".format([i for i in top_rated_query.limit(4)]))

top_rated_content_ids = [
    i[0] for i in top_rated_query.values(MoviesTop.movieid)
][:5]
# --------------------------------------------------------------

# ������� �� PyMongo
mongo = MongoClient(**{
    'host': os.environ['APP_MONGO_HOST'],
    'port': int(os.environ['APP_MONGO_PORT'])
})

# �������� ������ � ��������� tags
db = mongo["movie"]
tags_collection = db['tags']

# id �������� ����������� ��� ���������� - ��������� ��� � ����������� $in ������ find
# � ������� ������ ������ ���� ������� �� ������� top_rated_content_ids
mongo_query = tags_collection.find(
        {'id': {}}
)
mongo_docs = [
    i for i in mongo_query
]

print("������� ��������� �� Mongo: {}".format(mongo_docs[:5]))

id_tags = [(i['id'], i['name']) for i in mongo_docs]


# ������� �� Pandas
# --------------------------------------------------------------
# ��������� ������� �� ����� � ���������� top-5 ����� ����������

# ��������� DataFrame
tags_df = pd.DataFrame(id_tags, columns=['movieid', 'tags'])

# --------------------------------------------------------------
# ��� ��� �����
# ������������ �� �������� ���� � ������� group_by
# ��� ������� ���� ���������, � ����� ���������� ������� �� �����������
# �������� top-5 ����� ���������� �����

top_5_tags = tags_df.head(5)

print(top_5_tags)

logger.info("������� ���������!")
# --------------------------------------------------------------