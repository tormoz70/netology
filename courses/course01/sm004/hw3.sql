SELECT 'ФИО: Халиуллин Айрат';

DROP TABLE IF EXISTS keywords;

\! echo "Creating table keywords..."
CREATE TABLE keywords
(
  id numeric (18) NOT NULL,
  tags text NOT NULL
);
\! echo "Table keywords created."

\! echo "Loading data into table keywords..."
\copy keywords FROM '/data/keywords.csv' DELIMITER ',' CSV HEADER
\! echo "Data loaded."


DROP TABLE IF EXISTS top_rated_tags;

\! echo "Creating table top_rated_tags..."
WITH movies0 AS (
	SELECT a.movieid,
		avg(a.rating) as avg_rating 
	FROM ratings a
	GROUP BY a.movieid
	HAVING count(distinct a.userid) > 50
	ORDER BY avg_rating DESC, movieid ASC
)
SELECT a.movieid, k.tags INTO top_rated_tags
  FROM movies0 a
	INNER JOIN keywords k ON k.id = a.movieid
LIMIT 150;
\! echo "Table top_rated_tags created."


\! echo "Exporting table top_rated_tags..."
\copy (SELECT * FROM top_rated_tags) TO '/data/top_rated_tags.csv' WITH CSV HEADER DELIMITER as E'\t';
\! echo "Table top_rated_tags exported."

