SELECT a.userid,  a.movieid, a.rating,
	case when (max(a.rating) over w - min(a.rating) over w) != 0 then 
		round(((a.rating - min(a.rating) over w)/(max(a.rating) over w - min(a.rating) over w))::numeric, 3)
	else 0 end as normed_rating,
	round((avg(a.rating) over w)::numeric, 3) as avg_rating 
  FROM ratings a
WINDOW w AS (partition by a.userid)
LIMIT 30;
