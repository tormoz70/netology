﻿DROP FUNCTION cross_arr (arr1 int[], arr2 int[]);

CREATE OR REPLACE FUNCTION cross_arr (arr1 numeric[], arr2 numeric[]) RETURNS numeric[] 
as 
$FUNCTION$ 
with commons as (
	select UNNEST(arr1) as arrItem
	intersect 
	select UNNEST(arr2) as arrItem
)
select array_agg(arrItem) as intersectItems from commons

$FUNCTION$
language sql;

CREATE OR REPLACE FUNCTION diff_arr (arr1 numeric[], arr2 numeric[]) RETURNS numeric[] 
as 
$FUNCTION$ 
with commons as (
	select UNNEST(arr1) as arrItem
	except 
	select UNNEST(arr2) as arrItem
)
select array_agg(arrItem) as exceptItems from commons

$FUNCTION$
language sql;



with ddd as (
SELECT urole_id, array_agg(ugrant_id) as ugrants
  FROM efond2_usr.grantofrole
 GROUP BY urole_id
)
,rrr1 as (
select urole_id, ugrants from ddd --where urole_id = 16
)
,rrr2 as (
select urole_id, ugrants from ddd  --where urole_id = 10
)
select a.urole_id as urole_id1, a.ugrants as ugrants1, b.urole_id as urole_id2, b.ugrants as ugrants2,
		cross_arr(a.ugrants, b.ugrants) as cmmn,
		diff_arr(a.ugrants, b.ugrants) as diff
from rrr1 a
	cross join rrr2 b
where a.urole_id != b.urole_id
