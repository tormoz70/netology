SELECT '���: ��������� �����'; 

\! echo "2. ������������� ������� Postgres" 


\! echo "Creating small_ratings..."
DROP TABLE IF EXISTS small_ratings;
CREATE TABLE small_ratings AS 
SELECT *
  FROM ratings 
 LIMIT 10000;
\! echo "small_ratings - created."


\! echo "Creating user_movies_agg..."
DROP TABLE IF EXISTS user_movies_agg;

CREATE TABLE user_movies_agg AS 
SELECT userID, array_agg(movieId) as user_views 
  FROM small_ratings 
 GROUP BY userID;
\! echo "user_movies_agg - created."


\! echo "Creating function cross_arr..."
CREATE OR REPLACE FUNCTION cross_arr (arr1 bigint[], arr2 bigint[]) RETURNS bigint[] 
as 
$BODY$ 
with commons as (
	select UNNEST(arr1) as arrItem
	intersect 
	select UNNEST(arr2) as arrItem
)
select array_agg(arrItem) as intersectItems from commons

$BODY$
language sql;
\! echo "function cross_arr - created."

\! echo "Creating common_user_views..."
DROP TABLE IF EXISTS common_user_views;
\timing
CREATE TABLE common_user_views AS
select a.userID u1, b.userID u2, 
       cross_arr(a.user_views, b.user_views) as intersects
from user_movies_agg a
	cross join user_movies_agg b
where a.userID != b.userID;
\timing
\! echo "common_user_views - created."

\! echo "Selecting top-10 of common_user_views..."
with top0 as (
	select a.*,
		array_length(a.intersects, 1) intersects_len
	  from common_user_views a
	where a.intersects is not null
	order by intersects_len desc
)
select * from top0 limit 10;
\! echo "top-10 - selected."

\! echo "Creating function diff_arr..."
CREATE OR REPLACE FUNCTION diff_arr (arr1 bigint[], arr2 bigint[]) RETURNS bigint[] 
as 
$BODY$ 
with commons as (
	select UNNEST(arr1) as arrItem
	except 
	select UNNEST(arr2) as arrItem
)
select array_agg(arrItem) as exceptItems from commons

$BODY$
language sql;
\! echo "function diff_arr - created."


\! echo "Building recommendations for u1..."

SELECT a.u1,
	diff_arr(b2.user_views, a.intersects) as recommendations
  FROM common_user_views a
  	INNER JOIN user_movies_agg b2 ON b2.userId = a.u2
LIMIT 10; 

\! echo "All done."
