﻿CREATE USER ololouser WITH PASSWORD 'ololopass';
CREATE DATABASE ololodb;
GRANT ALL PRIVILEGES ON DATABASE ololodb TO ololouser;
CREATE TABLE account(user_id serial PRIMARY KEY, email VARCHAR (355) UNIQUE NOT NULL, last_login TIMESTAMP);

INSERT INTO account VALUES (123, 'ololo@ya.ru', '2003-2-1'::timestamp);
SELECT * FROM account;

INSERT INTO account(email, last_login) VALUES ('ololo1@ya.ru', '2003-2-1'::timestamp);
INSERT INTO account(email, last_login) VALUES ('ololo11@ya.ru', '2003-2-1'::timestamp);

ALTER TABLE account ADD COLUMN phone VARCHAR;

UPDATE account SET phone=md5(random()::text) where phone is null;

SELECT * FROM account;
