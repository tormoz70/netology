﻿create schema NLHW_KINOPOISK;

drop table if exists NLHW_KINOPOISK.FILMES;
create table NLHW_KINOPOISK.FILMES (
	film_id numeric not null,
	title text not null,
	country text not null,
	box_office numeric not null default 0,
	release_year date not null,
  constraint PK_FILMES PRIMARY KEY (film_id)
);

drop sequence if exists NLHW_KINOPOISK.FILMES_SEQ;
create sequence NLHW_KINOPOISK.FILMES_SEQ increment by 1 start with 0 minvalue 0 no maxvalue no cycle cache 1;

insert into NLHW_KINOPOISK.FILMES values(nextval('NLHW_KINOPOISK.FILMES_SEQ'), 'Побег из Шоушенка', 'США', 59841469, to_date('1994', 'YYYY'));
insert into NLHW_KINOPOISK.FILMES values(nextval('NLHW_KINOPOISK.FILMES_SEQ'), 'Зеленая миля', 'США', 286801374, to_date('1999', 'YYYY'));
insert into NLHW_KINOPOISK.FILMES values(nextval('NLHW_KINOPOISK.FILMES_SEQ'), 'Форрест Гамп', 'США', 677386686, to_date('1994', 'YYYY'));
insert into NLHW_KINOPOISK.FILMES values(nextval('NLHW_KINOPOISK.FILMES_SEQ'), 'Леон', 'Франция', 19501238, to_date('1994', 'YYYY'));
insert into NLHW_KINOPOISK.FILMES values(nextval('NLHW_KINOPOISK.FILMES_SEQ'), 'Начало', 'США,Великобритания', 825532764, to_date('2010', 'YYYY'));

drop table if exists NLHW_KINOPOISK.PERSONS;
create table NLHW_KINOPOISK.PERSONS (
	person_id numeric not null,
	fio text not null,
  constraint PK_COUNRIES PRIMARY KEY (person_id)
);

create unique index PERSONS_FIO_UI on NLHW_KINOPOISK.PERSONS (fio);
  
drop sequence if exists NLHW_KINOPOISK.PERSONS_SEQ;
create sequence NLHW_KINOPOISK.PERSONS_SEQ increment by 1 start with 0 minvalue 0 no maxvalue no cycle cache 1;

insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Фрэнк Дарабонт');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Стивен Кинг');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Ники Марвин');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Лиз Глоцер');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Дэвид В. Лестер');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Роджер Дикинс');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Томас Ньюман');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Теренс Марш');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Питер Лэндсдаун Смит');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Элизабет МакБрайд');

insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Дэвид Валдес');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Дэвид Тэттерсолл');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Уильям Крус'); 
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Керин Вагнер');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Ричард Фрэнсис-Брюс');


insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Роберт Земекис');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Эрик Рот');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Уинстон Грум');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Венди Файнерман');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Стив Старки');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Стив Тиш');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Дон Бёрджесс');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Алан Сильвестри');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Рик Картер');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Лесли МакДональд');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Уильям Джеймс Тигарден');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Артур Шмидт');

insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Люк Бессон');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Клод Бессон');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Джон Гарлэнд');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Бернард Гренет');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Тьерри Арбога');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Эрик Серра');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Дэн Вейл');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Жерар Дролон');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Магали Гвидаси');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Сильви Ландра');

insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Кристофер Нолан');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Эмма Томас');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Закария Алауи');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Уолли Пфистер');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Ханс Циммер');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Гай Диас');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Люк Фриборн');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Брэд Рикер');
insert into NLHW_KINOPOISK.PERSONS values(nextval('NLHW_KINOPOISK.PERSONS_SEQ'), 'Ли Смит');

drop table if exists NLHW_KINOPOISK.PERSONS2CONTENT;
create table NLHW_KINOPOISK.PERSONS2CONTENT (
	person_id numeric not null,
	film_id numeric not null,
	person_type text not null,
  constraint PK_PERSONS2CONTENT PRIMARY KEY (person_id, film_id, person_type)
);


select * from NLHW_KINOPOISK.FILMES;
select * from NLHW_KINOPOISK.PERSONS;

insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'режиссер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Фрэнк Дарабонт';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'сценарий', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Фрэнк Дарабонт';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'сценарий', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Стивен Кинг';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Ники Марвин';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Лиз Глоцер';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Дэвид В. Лестер';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'оператор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Роджер Дикинс';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'композитор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Томас Ньюман';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Теренс Марш';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Питер Лэндсдаун Смит';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 0, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Элизабет МакБрайд';

insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'режиссер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Фрэнк Дарабонт';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'сценарий', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Фрэнк Дарабонт';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'сценарий', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Стивен Кинг';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Фрэнк Дарабонт';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Дэвид Валдес';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'оператор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Дэвид Тэттерсолл';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'композитор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Томас Ньюман';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Теренс Марш';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Уильям Крус';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Керин Вагнер';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 1, 'монтаж', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Ричард Фрэнсис-Брюс';

insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'режиссер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Роберт Земекис';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'сценарий', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Эрик Рот';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'сценарий', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Уинстон Грум';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Венди Файнерман';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Стив Старки';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Стив Тиш';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'оператор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Дон Бёрджесс';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'композитор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Алан Сильвестри';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Рик Картер';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Лесли МакДональд';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Уильям Джеймс Тигарден';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 2, 'монтаж', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Артур Шмидт';


insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'режиссер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Люк Бессон';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'сценарий', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Люк Бессон';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Клод Бессон';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Джон Гарлэнд';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Бернард Гренет';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'оператор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Тьерри Арбога';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'композитор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Эрик Серра';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Дэн Вейл';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Жерар Дролон';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Магали Гвидаси';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 3, 'монтаж', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Сильви Ландра';

insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'режиссер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Кристофер Нолан';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'сценарий', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Кристофер Нолан';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Кристофер Нолан';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Эмма Томас';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'продюсер', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Закария Алауи';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'оператор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Уолли Пфистер';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'композитор', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Ханс Циммер';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Гай Диас';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Люк Фриборн';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'художник', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Брэд Рикер';
insert into NLHW_KINOPOISK.PERSONS2CONTENT(film_id, person_type, person_id) select 4, 'монтаж', p.person_id from NLHW_KINOPOISK.PERSONS p where p.fio ilike 'Ли Смит';

select * from NLHW_KINOPOISK.PERSONS2CONTENT;