SELECT '���: ��������� �����';

DROP TABLE IF EXISTS links_parted;
CREATE TABLE links_parted (
 movieid bigint not null,
 imdbid  character varying(20) not null,
 tmdbid  character varying(20) not null
);

DROP TABLE IF EXISTS links_parted_0;
CREATE TABLE links_parted_0 (
    CHECK (movieid % 2 = 0)
) INHERITS (links_parted);

DROP TABLE IF EXISTS links_parted_1;
CREATE TABLE links_parted_1 (
    CHECK (movieid % 2 = 1)
) INHERITS (links_parted);

CREATE OR REPLACE RULE links_insert_0 AS ON INSERT TO links_parted
WHERE ( movieid % 2 = 0 )
DO INSTEAD INSERT INTO links_parted_0 VALUES ( NEW.* );

CREATE OR REPLACE RULE links_insert_1 AS ON INSERT TO links_parted
WHERE ( movieid % 2 = 1 )
DO INSTEAD INSERT INTO links_parted_1 VALUES ( NEW.* );

insert into links_parted(
	select * from links
);
