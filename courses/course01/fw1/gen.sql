select * from givc_org.org_act_attrs;

drop table DEVKU.THEATRES;
create table DEVKU.THEATRES as
select 
    a.id_org as theatre_id,
    a.orgname as theatre_name,
    a.orgtype,
    a.holding_id,
    aa.kladr_rg_name as region,
    aa.kladr_np_name as city
from org$r a
    inner join givc_org.org_act_attrs aa on aa.id_org = a.id_org
where a.orgtype = 'h'
and a.id_org in (1670,1577,1740,70,1315)
and a.state = 0
union all
select 
    a.id_org as theatre_id,
    a.orgname as theatre_name,
    a.orgtype,
    a.holding_id,
    aa.kladr_rg_name as region,
    aa.kladr_np_name as city
from org$r a
    inner join givc_org.org_act_attrs aa on aa.id_org = a.id_org
where a.orgtype = 'p'
and a.holding_id in (1670,1577,1740,70,1315)
and a.state = 0
/

select * from DEVKU.THEATRES where orgtype = 'h' order by 2;

select * from DEVKU.THEATRES where orgtype = 'p' order by 2;

select * from DEVKU.THEATRES;

drop table DEVKU.HALLS;
create table DEVKU.HALLS as 
select a.showr_id as hall_id,
       a.org_id as theatre_id,
       a.sname as hall_name,
       a.places,
       a.exp_3d,
       a.exp_imax,
       a.explaser as exp_laser,
       a.exp_dvd
from givcadmin.showroom_act a
inner join DEVKU.THEATRES b on b.theatre_id = a.org_id
/

select * from DEVKU.HALLS;

select a.hall_id, count(1) cnt 
from DEVKU.HALLS a
 group by a.hall_id
 having count(1) > 1;

select count(1) from DEVKU.HALLS;

drop table DEVKU.SESSIONS;
create table DEVKU.SESSIONS as
select 
    a.sess_id,
    a.show_date,
    a.org_id as theatre_id,
    a.sroom_id as hall_id,
    min(a.pu_num_found) as film_id,
    sum(round(a.tckts*dbms_random.value(0.7, 1.0))) as tckts,
    sum(round(a.summ*dbms_random.value(0.7, 1.0))) as summ
from cub$sess a
    inner join DEVKU.THEATRES t on t.theatre_id = a.org_id
    inner join DEVKU.HALLS h on h.hall_id = a.sroom_id
    --inner join DEVKU.FILMS f on f.film_id = a.pu_num_found
where a.part_key between '20151101' and '20160331'
  and a.pu_num_found is not null
group by a.sess_id, a.show_date, a.org_id, a.sroom_id
/

select count(1) from DEVKU.SESSIONS;

select sess_id, count(1) from DEVKU.SESSIONS
group by sess_id
having count(1) > 1;


select theatre_id,  count(1) from DEVKU.SESSIONS 
where theatre_id in (2751,1316)
group by theatre_id;

select * from DEVKU.SESSIONS
where sess_id = 34696364;

select * from DEVKU.SESSIONS s
where s.show_date BETWEEN to_date('2015-12-31T00:00:00', 'YYYY-MM-DD"T"HH24:MI:SS') AND to_date('2015-12-31T23:59:59', 'YYYY-MM-DD"T"HH24:MI:SS')
/

select * from DEVKU.SESSIONS;

drop table DEVKU.FILMS;
create table DEVKU.FILMS as
select 
    a.pu_num as film_id,
    a.name_rus as film_name,
    a.prod_year,
    a.madein,
    a.studia, 
    a.startdate,
    a.age_restr,
    a.genre,
    a.mdirector
  from givcadmin.cub5$kinos a
WHERE exists (select 1 from DEVKU.SESSIONS b where b.film_id = a.pu_num);
 
select * from DEVKU.FILMS;


