SELECT '���: ��������� �����';

-- 1. ������� �������
\! echo "������ 1.1"
SELECT * FROM ratings LIMIT 10;

\! echo "������ 1.2"
SELECT * FROM links a
WHERE a.imdbid like '%42'
  AND a.movieid between 100 and 1000
LIMIT 10;

-- 2. ������� �������: JOIN
\! echo "������ 2.1"
SELECT a.imdbid FROM links a
  INNER JOIN ratings b ON b.movieid=a.movieid
WHERE b.rating = 5
LIMIT 10;

-- 3. ���������� ������: ������� ����������
\! echo "������ 3.1"
SELECT count(distinct a.movieid) FROM links a
WHERE NOT EXISTS (SELECT 1 FROM ratings b WHERE b.movieid=a.movieid);

\! echo "������ 3.2"
SELECT a.userid, avg(a.rating) 
  FROM ratings a
GROUP BY a.userid
HAVING avg(a.rating) > 3.5
ORDER BY 2 DESC
LIMIT 10;


-- 4. ������������� �������
\! echo "������ 4.1"
SELECT avg(a.rating) FROM ratings a
  INNER JOIN links l ON l.movieid=a.movieid
  INNER JOIN (SELECT l.imdbid, avg(a.rating) 
                FROM ratings a
                  INNER JOIN links l ON l.movieid=a.movieid
               GROUP BY l.imdbid
              HAVING avg(a.rating) > 3.5
               ORDER BY 2 DESC
               LIMIT 10) r ON r.imdbid = l.imdbid;


\! echo "������ 4.2"
WITH usrs AS (
  SELECT a.userid, count(1) cnt
    FROM ratings a
   GROUP BY a.userid
  HAVING count(1) > 10
)
SELECT avg(a.rating) FROM ratings a
 WHERE a.userid IN (SELECT u.userid FROM usrs u);
 
